# Proseminar Templates Concepts and Constexpr 2022

In this repository, we offer all the examples used in our elaboration, as well as some code you can experiment with. You can also find the elaboration here. 
To make navigating easier we will explain the directories:


- Storage:
    Our introductionary example contains:
    
    - storage_cpp:
    The class template for our Storage class is in Storage.hpp as well as a generic print function to avoid the std:: name space for understanding reason
    In main.cpp is a simple test for the class with different types.    

    - storage_java
    A java implementation of our class with a test in the main method

- StoreGreatest:
    This is our main example and the most elaborate one.
    This dictionary contains:
    
    - StoreGreatest_c++:
    The basic StoreGreatest class in the .hpp class as explained in presentation and elaboration.
    A simple test of our container class in the  .cpp.

    - StoreGreatest_c++_advanced_version 
    (not used during presentation)
        Integer.cpp and .hpp are advanced and mostly interesting to understand the advanced version of the StoreGreatest.hpp file which is tested in the main.cpp file
    
    - StoreGreatest_c++_error_without_concept
    (only errors used during presentation)
        Shows the difference in errors with or without concept. If you research online, you will find how error messages  grow exponetially in size without concepts. Give it a try!
    
    - StoreGreatest_c++_interface_no_concept
    (not used during presentation)
        This is a solution avoiding (self-written) concepts and is similar to a "Java-like" approach
        It uses an class (Comparable.hpp) from which all generic types we our  container shall handle MUST be derived (StoreGreatest.hpp)

    - StoreGreatest_c++_specialization
        In the .hpp is the classic template for the class.
        In the main.cpp file, there is a total template specialization for "int"

    - StoreGreatest_Java
    (not used during presentation)
        Java implementation of the StoreGreatest class. All generic types must extend Comparable

- sequences(not used during presentation) 
    here, we show the (run-time) optimizing properties of constexpr with known mathematical sequences

- constexprArray_powerset
    determining the size of an array representing a power set of another array by using constexpr  
    main.cpp illustrates the use of constexpr
    print.hpp allows to again use our print function

- constexprConstructor
    Shows an implementation of our own vector class allowing to construct them during compilation.

- constructable_from
    concept to see whether the first type parameter has a constructor which accepts:
        - Constr_From_TwoParameters: the second type parameter
        - Constr_From_ParameterPacks: the remaining type parameters

- sumFunction_ParameterPacks
    recursive function to add one or more arguments of unspecified type,
    misses proper concept to avoid missing "+" operation between arguments
    print.hpp introduces the print function
    .cpp file implements and tests sumFunction