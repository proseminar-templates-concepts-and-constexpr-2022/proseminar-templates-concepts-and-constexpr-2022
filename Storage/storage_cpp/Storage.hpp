#pragma once
#include <iostream>

template<typename T>
class Storage {
private:
    T m_element;
public:
    Storage(T element):m_element(element){}

    T getElement() {
        return m_element;
    }

    /**
     * Replace the current stored element with the new one
     * @param element the element that will be stored
     * @return the previously stored element
     */
    T add(T element){
        T tempElement = m_element;
        m_element = element; // copy the element into m_element
        return tempElement;
    }
};

template<typename T>
concept Printable = requires(T out){
    std::cout << out;
};

template<Printable T>
void print(T output){
    std::cout << output << std::endl;
}