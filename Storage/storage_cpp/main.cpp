#include <iostream>
#include "Storage.hpp"

using namespace std;

int main(){
    // Example 1: Storing data of type int in data structure Storage
    print("---------------- Example 1 ----------------");
    Storage<int> k(0);
    print(k.getElement());

    int i = 3;
    k.add(i);
    print(k.getElement());

    k.add(2);
    print(k.getElement());

    
    // Example 2: Storing data of type string in data structure Storage
    print("---------------- Example 2 ----------------");
    Storage<string> k2("Hallo");
    print(k2.getElement());

    k2.add("Welt");
    print(k2.getElement());

    k2.add("Test");
    print(k2.getElement());  
}