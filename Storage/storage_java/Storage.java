public class Storage<T> {

    private T element;

    public Storage(T element){
        this.element = element;
    }

    public T getElement(){
        return this.element;
    }

    /**
     * Replace the current stored element with the new one
     * @param element the element that will be stored
     * @return the previously stored element
     */
    public T add(T element){
        T tempElement = this.element;
        this.element = element;
        return tempElement;
    }

    public static void main(String[] args){
        // Example 1: Storing data of type Integer in data structure Storage
        System.out.println("---------------- Example 1 ----------------");
        Storage<Integer> k = new Storage<>(0);
        System.out.println(k.getElement());

        Integer i = 3;
        k.add(i);
        System.out.println(k.getElement());

        k.add(2);
        System.out.println(k.getElement());

        
        // Example 2: Storing data of type String in data structure Storage
        System.out.println("---------------- Example 2 ----------------");
        Storage<String> k2 = new Storage<>("Hallo");
        System.out.println(k2.getElement());

        k2.add("Welt");
        System.out.println(k2.getElement());

        k2.add("Test");
        System.out.println(k2.getElement());        
    }
}
