public class StoreGreatest<T extends java.lang.Comparable<T>>{

    private T greatest;

    public StoreGreatest(T element){
        this.greatest = element;
    }

    public T getGreatest(){
        return this.greatest;
    }

    /**
     * Replace the current greatest element with the new one if it is greater then the current greatest element
     * @param element the element that may be added
     * @return the smaller of the two elements: the current greatest element and that one specified as parameter 
     */
    public T add(T element){
        // store element in this.greatest if element is greater than this.greatest 
        if(element.compareTo(this.greatest) > 0) { 
            T tempGreatest = this.greatest;
            this.greatest = element;
            return tempGreatest;
        }
        return element;
    }

    public static void main(String[] args){
        // Example 1: Storing data of type Integer in data structure StoreGreatest
        System.out.println("---------------- Example 1 ----------------");
        StoreGreatest<Integer> k = new StoreGreatest<>(0);
        System.out.println(k.getGreatest());

        Integer i = 3;
        k.add(i);
        System.out.println(k.getGreatest());

        k.add(2);
        System.out.println(k.getGreatest());

        
        // Example 2: Storing data of type String in data structure StoreGreatest
        System.out.println("---------------- Example 2 ----------------");
        StoreGreatest<String> k2 = new StoreGreatest<>("Hallo");
        System.out.println(k2.getGreatest());

        k2.add("Welt");
        System.out.println(k2.getGreatest());

        k2.add("Test");
        System.out.println(k2.getGreatest());        
    }
}
