#include "Integer.hpp"

/**
 * NOTE: This class only serves the purpose of better understanding Move Semantics and the assignment and copying of objects.
 * This way you can easily see where Integer objects were created, copied or moved. 
 * This class is explicitly not meant to be a good implementation of an integer!
 */

Integer::Integer(int v){ // ordinary constructor
    m_value = new int;
    *m_value = v;
    std::cout << "Info: Created " << *this << std::endl;
}

Integer::Integer(const Integer& other){ // copy construcor
    m_value = new int;
    *m_value = *other.m_value;
    std::cout << "Info: " << *this << " Copy Constructor" << std::endl;
}

Integer::Integer(Integer&& other) noexcept { // move constructor
    m_value = other.m_value;
    other.m_value = nullptr;
    std::cout << "Info: " << *this << " Move Constructor" << std::endl;
}

Integer::~Integer(){
    std::cout << "Info: Destroyed " << *this << std::endl;
    delete m_value;
}

Integer& Integer::operator=(Integer&& other) noexcept { // move assignment operator

    if (this == &other)
        return *this;

    delete m_value;
    m_value = other.m_value;
    other.m_value = nullptr;
    
    std::cout << "Info: " << *this << " Move Assignment" << std::endl;
    return *this;
}

Integer& Integer::operator=(const Integer& other){ // copy assignment operator
    if (this == &other)
        return *this;

    *m_value = *other.m_value;
    std::cout << "Info: " << *this << " Copy Assignment" << std::endl;
    return *this;
}

bool Integer::operator<(const Integer& other){ // operator overloading
    if (m_value == nullptr) return true;
    return (*m_value < other.getValue());
}

int Integer::getValue() const{
    if(m_value != nullptr) return *m_value;
    else return 0; // never happens
}

std::ostream& operator<<(std::ostream& stream, const Integer& t){ // overloading the stream insertion operator to be able to print objects of type Integer to the console
    stream << "Integer(" << t.getValue() << ")";
    return stream;
}