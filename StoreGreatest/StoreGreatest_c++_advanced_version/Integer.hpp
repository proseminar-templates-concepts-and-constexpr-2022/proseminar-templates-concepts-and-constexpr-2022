#pragma once
#include <iostream>

/**
 * NOTE: This class only serves the purpose of better understanding Move Semantics and the assignment and copying of objects.
 * This way you can easily see where Integer objects were created, copied or moved. 
 * This class is explicitly not meant to be a good implementation of an integer!
 */

class Integer{
private:
    int* m_value;
public:
    Integer(int v); // ordinary constructor
    Integer(const Integer& other); // copy construcor
    Integer(Integer&& other) noexcept; // move constructor
    ~Integer(); // destructor
    Integer& operator=(Integer&& other) noexcept; // move assignment operator
    Integer& operator=(const Integer& other); // copy assignment operator
    bool operator<(const Integer& other); // operator overloading
    int getValue() const;
};

std::ostream& operator<<(std::ostream& stream, const Integer& t); // overloading the stream insertion operator to be able to print objects of type Integer to the console