#pragma once
#include <concepts>
#include <optional>

template<typename T>
concept Comparable = requires(T a, T b){
    {a < b} -> std::same_as<bool>;
};

template<typename T> 
    requires Comparable<T>
class StoreGreatest {
private:
    std::optional<T> m_greatest;
public:
    StoreGreatest():m_greatest(std::nullopt){} // ordinary constructor

    StoreGreatest(const T& element){
        m_greatest.emplace(element);
    }

    StoreGreatest(T&& element) :m_greatest(std::move(element)){}

    StoreGreatest(const StoreGreatest& other){ // copy constructor
        if(other.getGreatest()) m_greatest.emplace(*other.getGreatest());
        else m_greatest = std::nullopt;
    }

    const std::optional<T>& getGreatest() const {
        return m_greatest;
    }

    /**
     * Replace the current greatest element with the new one if it is greater then the current greatest element
     * @param element the element that may be added
     * @return if StoreGreatest is not empty, the smaller of the two elements (the current greatest element and that one specified as parameter). Otherwise std::nullopt will be returned
     */
    std::optional<T> add(const T& element){
        if(!m_greatest){
            m_greatest.emplace(element); // copy the element into m_greatest
            return std::nullopt;
        }
        else if(*m_greatest < element){
            const T temp = std::move(*m_greatest);
            m_greatest.emplace(element); // copy the element into m_greatest
            return temp;
        }
        return element;
    }

    /**
     * Replace the current greatest element with the new one if it is greater then the current greatest element
     * @param element the element that may be added
     * @return if StoreGreatest is not empty, the smaller of the two elements: the current greatest element and that one specified as parameter. Otherwise std::nullopt will be returned 
     */
    std::optional<T> add(T&& element){
        if(!m_greatest){
            m_greatest = std::move(element);
            return std::nullopt;
        }
        else if(*m_greatest < element){
            const T temp = std::move(*m_greatest);
            m_greatest = std::move(element);
            return temp;
        }
        return element;
    }

    void reset(){
        m_greatest = std::nullopt;
    }

    bool isEmpty(){
        return !m_greatest.has_value();
    }
};