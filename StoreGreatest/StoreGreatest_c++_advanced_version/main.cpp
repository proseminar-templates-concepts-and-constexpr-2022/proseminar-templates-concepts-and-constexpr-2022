#include <iostream>
#include "StoreGreatest.hpp"
#include "Integer.hpp"

/**
 * NOTE: The following code serves the purpose of showing the use of the data structure "StoreGreatest" as an example. 
 * A class Integer is used, which only serves the purpose of a better understanding of Move Semantics and the assignment and copying of objects. 
 * Feel free to play around with this example, StoreGreatest and different types on it.
 */

int main(){

    // Example 1: Storing data of type Integer in data structure StoreGreatest
    std::cout << "---------------- Example 1 ----------------" << std::endl;
    StoreGreatest<Integer> k(Integer(2));

    Integer s = Integer(3);
    k.add(s);

    const std::optional<Integer>& a = k.getGreatest();
    if(a) std::cout << "Output: " << *a << std::endl;

    k.add(Integer(1));
    if (k.getGreatest()) std::cout << "Output: " << *k.getGreatest() << std::endl;

    StoreGreatest<Integer> k2;
    k2.add(Integer(4));

    std::cout << "hier" << std::endl;
}