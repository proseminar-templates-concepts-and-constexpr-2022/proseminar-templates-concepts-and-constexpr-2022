#include<iostream>
#include<concepts>

template<typename T>
concept Comparable = requires(T a, T b){
    {a < b} -> std::same_as<bool>;
};

template<typename T> 
    //requires Comparable<T> // without this statement line 39 will cause an error
class StoreGreatest {
private:
    T greatestElement;
public:
    StoreGreatest(T init):greatestElement(init){}

    T getGreatest() {return greatestElement;}  

    T add(T newElement){
        if(greatestElement < newElement) {
            T tempGreatest = greatestElement;
            greatestElement = newElement;
            return tempGreatest;
        }
        else return newElement;
    }
};

class Vector{
public:
    double x;
    double y;
};

int main(){
    Vector v1;
    Vector v2;
    StoreGreatest<Vector> e1(v1);
    // e1.add(v2); // because there is no "<" in class Vector: "if(greatestElement < newElement)" can not be evaluated
}

