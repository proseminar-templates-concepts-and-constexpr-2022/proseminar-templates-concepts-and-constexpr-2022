#pragma once
#include <iostream>
#include <concepts>
#include "Comparable.hpp"

template<typename T>
concept ComparableConcept = std::derived_from<T, Comparable<T>>;

template<typename T> 
    requires ComparableConcept<T>
class StoreGreatest {
private:
    T m_greatest;
public:
    StoreGreatest(T element):m_greatest(element){}

    T getGreatest() {
        return m_greatest;
    }

    /**
     * Replace the current greatest element with the new one if it is greater then the current greatest element
     * @param element the element that may be added
     * @return the smaller of the two elements: the current greatest element and that one specified as parameter 
     */
    T add(T element){
        if(element.compareTo(m_greatest) > 0) {
            T tempGreatest = m_greatest;
            m_greatest = element; // copy the element into m_greatest
            return tempGreatest;
        }
        return element;
    }
};

template<typename T>
concept Printable = requires(T out){
    std::cout << out;
};

template<Printable T>
void print(T output){
    std::cout << output << std::endl;
}