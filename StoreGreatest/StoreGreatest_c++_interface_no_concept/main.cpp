#include <cmath>
#include "StoreGreatest.hpp"
#include "Comparable.hpp"

using namespace std;

/**
 * NOTE: The following code serves the purpose of showing the use of the data structure "StoreGreatest" as an example.  
 * Feel free to play around with this example, StoreGreatest and different types on it. 
 * Note that types used in StoreGreatest must inherit from the Comparable interface in "Comparable.hpp".
 */

class Vector2 : public Comparable<Vector2>{
public:
    double x;
    double y;
    Vector2(double pX, double pY){
        x = pX;
        y = pY;
    }
    double norm(){
        return sqrt(x*x + y*y);
    }

    virtual int compareTo(Vector2 other){
        double res = norm() - other.norm();
        if(res == 0) return 0;
        else if (res < 0) return -1;
        else return 1;
    } 
};

int main(){

    // Example 1: Storing data of type Vector2 in data structure StoreGreatest
    print("---------------- Example 1 ----------------");
    StoreGreatest<Vector2> k3(Vector2(1.0, 1.0));

    Vector2 vec = Vector2(4.0, 3.0);
    k3.add(vec);
    k3.add(Vector2(2.0, 4.5));

    print("x: ");
    print(k3.getGreatest().x);
    print("y: ");
    print(k3.getGreatest().y);

}