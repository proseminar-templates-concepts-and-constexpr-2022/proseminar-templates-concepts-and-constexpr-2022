#include <iostream>
#include <string>
#include <cmath>
#include "StoreGreatest.hpp"

using namespace std;

// Specialization of StoreGreatest
template<> 
class StoreGreatest<int> {
private:
    int m_greatest;
public:
    StoreGreatest(int element):m_greatest(element){
        print("Initialize data structure");
    }

    int getGreatest() {
        print("Return the stored element");
        return m_greatest;
    }

    /**
     * Replace the current greatest element with the new one if it is greater then the current greatest element
     * @param element the element that may be added
     * @return the smaller of the two elements: the current greatest element and that one specified as parameter 
     */
    int add(int element){
        if(m_greatest < element) {
            int tempGreatest = m_greatest;
            m_greatest = element; // copy the element into m_greatest
            return tempGreatest;
        }
        else return element;
    }
};


/**
 * NOTE: The following code serves the purpose of showing the use of the data structure "StoreGreatest" as an example.  
 * Feel free to play around with this example, StoreGreatest and different types on it.
 */

class Vector2{
public:
    double x;
    double y;
    Vector2(double pX, double pY){
        x = pX;
        y = pY;
    }
    double norm(){
        return sqrt(x*x + y*y);
    }
    bool operator<(Vector2 other){
        return norm() < other.norm();
    }
};

int main(){

    // Example 1: Storing data of type int in data structure StoreGreatest
    print("---------------- Example 1 ----------------");
    StoreGreatest<int> k(0);
    print(k.getGreatest());

    int i = 3;
    k.add(i);

    int greatest = k.getGreatest();
    print(greatest);

    k.add(2);

    print(k.getGreatest());
    
    // Example 2: Storing data of type string in data structure StoreGreatest
    print("---------------- Example 2 ----------------");
    StoreGreatest<string> k2("Hallo");
    print(k2.getGreatest());

    k2.add("Welt");
    print(k2.getGreatest());
    
    k2.add("Test");
    print(k2.getGreatest());

    // Example 3: Storing data of type Vector2 in data structure StoreGreatest
    print("---------------- Example 3 ----------------");
    StoreGreatest<Vector2> k3(Vector2(1.0, 1.0));

    Vector2 vec = Vector2(4.0, 3.0);
    k3.add(vec);
    k3.add(Vector2(2.0, 4.5));

    print("x: ");
    print(k3.getGreatest().x);
    print("y: ");
    print(k3.getGreatest().y);

}