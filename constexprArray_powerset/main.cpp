#include <array>
#include <list>
#include "print.hpp"

using namespace std;

constexpr int pow(int b, int e){
    if(e == 0) return 1;
    return b*pow(b, e-1);
}

int main(){

    array<int, 2> numbers;
    numbers[0] = 1;
    numbers[1] = 2;
    for(int i = 0; i<numbers.size(); i++)
        print(numbers[i]);

    constexpr int powerSetSize = pow(2, numbers.size());
    array<list<int>, powerSetSize> powerSet; 
}