#include <array>

using namespace std;

class Vector2{
public:
    double x;
    double y;
    constexpr Vector2(double pX, double pY){
        x = pX;
        y = pY;
    }
};


int main(){
    constexpr Vector2 v(1, 2);
    constexpr double vx = v.x;
    array<int, (int)vx> a;
    static_assert(vx == 1.0, "v1.x should be 3.0");
    static_assert(a.size() == 1, "Size of a should be 3");
}