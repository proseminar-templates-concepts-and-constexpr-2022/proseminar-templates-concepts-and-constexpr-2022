#include "print.hpp"
//This class only exists to illustrate 
//constructableFrom with more than two parameters
class Vector2{
public:
    double x;
    double y;
    constexpr Vector2(double pX, double pY){
        x = pX;
        y = pY;
    }
};

template <class ClassName, typename... Args>
concept constructableFrom = requires (Args... arguments) 
{
	ClassName(arguments...);
};

int main(){
	print(constructableFrom<int>); //This is now allowed
	print(constructableFrom<Vector2, double, double>)

}