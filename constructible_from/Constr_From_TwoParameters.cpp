#include "print.hpp"
#include "StoreGreatest.hpp"
template <class ClassName, typename Argument>
concept constructableFrom = requires (Argument a){
    ClassName(a);
    };

int main(){
	print(constructableFrom<StoreGreatest<int>, int>);  //1 means true
	//print(constructableFrom<int>)                     //error even tho int does have empty constructor
}