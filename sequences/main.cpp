#include <iostream>

constexpr int pot(int b, int e){
    if(e == 0) return 1;
    return b*pot(b, e-1);
}

constexpr int quadrat(int b){
    return b*b;
}

constexpr int quadrat2(int b){
    return pot(b, 2);
}

constexpr int fac(int n){
    if(n == 1) return 1;
    return n*fac(n-1);
}

constexpr int fib(int n){
    if(n == 1 || n == 2) return 1;
    return fib(n-1) + fib(n-2);
}

constexpr int lastSquare(int n){ // a_n = (a_{n-1})^2 mit a_1 = 2;
    if(n == 1) return 2;
    return quadrat(lastSquare(n-1));
}

int main(){
    
}