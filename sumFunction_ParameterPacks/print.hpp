#pragma once
#include <iostream>

template<typename T>
concept Printable = requires(T out){
    std::cout << out;
};

template<Printable T>
void print(T output){
    std::cout << output << std::endl;
}