#include "print.hpp"
//Basisfall: Einzelnes Argument wird selbst zurück gegeben
template <typename T>
	double sum(T t) {
	return t;
}
// Rekursiv: Bei mehr als einer Zahl wird die erste 
// Zahl rekursiv zur Summe des Rests addiert:
template <typename T, typename... Rest>
	double sum(T t, Rest... rest) {
	return t + sum(rest...);
}

int main(){
	print(sum<int, double, float>(5.3,7.7,3.0));
	print(sum(3));
	print(sum('c', 3.0));	
}